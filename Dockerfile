FROM alpine:latest as build

# Download tacacs
# http://www.pro-bono-publico.de/projects/
# j

ARG EVENT_DRIVEN_SERVERS=DEVEL.202005021648

RUN echo "${SRC_HASH}  /tac_plus.tar.gz"

ADD http://www.pro-bono-publico.de/projects/src/$EVENT_DRIVEN_SERVERS.tar.bz2 /tac_plus.tar.gz

# Install required packages for build
RUN apk update && \
    apk add \
    make \
    #build-base \
    bzip2
    #gcc \
    #perl \ 
    #perl-digest-md5 \
    #perl-ldap \
    #perl-io-socket-ssl \
    #bash \
    #bzip2 \
    #libdigest-md5-perllibnet-ldap-perllibio-socket-ssl-perl \
    #perl-digest-md5 && \
    #rm /var/cache/apk/*

# Build TACACS plus
# tar xvf
RUN tar -xzf /tac_plus.tar.gz && \
    pwd && \
    cd /PROJECTS && \
    ./configure --prefix=/tacacs && \
    make && \
    make install

# Create clean image
FROM alpine:latest

COPY --from=build /tacacs /tacacs

# Install required packages
RUN apk update && \
    apk add \
    perl-digest-md5 \
    perl-ldap \
    rm /var/cache/apk/*

# Redirect logs to STDOUT
RUN ln -sf /dev/stdout /var/log/radius/radius.log

EXPOSE \
    1812/udp \
    1813/udp


#CMD ["radiusd","-xx","-f"]
CMD ["/tacacs/sbin/tac_plus","-xx","-f"]